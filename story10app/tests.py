from django.test import TestCase, override_settings, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from .models import *
from django.contrib.auth.models import User

class UnitTest(TestCase):
    def setUp(self):
        user = User.objects.create_user('foo', password='boo')
        user.is_staff=True
        user.save()
    def test_views_success(self):
        response = Client().post('')
        self.assertEqual(response.status_code, 200)
        response = Client().post('/signup/')
        self.assertEqual(response.status_code, 200)
    def test_login_logout(self):
        response = self.client.post('', {'username':'foo', 'password1':'boo'})
        self.assertContains(response, 'Selamat datang, foo')
    def test_logout(self):
        response = self.client.post('', {'username':'fqweqwoo', 'password1':'boo'})
        response = self.client.post('', {'logout':'logout'})
        self.assertNotContains(response, 'Selamat datang, foo')
    def test_sign_in(self):
        response = self.client.post('/signup/', {'username': 'TesUsername200', 'password1':'SuccessBefore',
        'password2':'SuccessBefore'}, follow = True)
        self.assertTemplateUsed(response, 'login.html')
        self.assertContains(response, 'Selamat datang, TesUsername200')

class FunctionalTest(LiveServerTestCase):
    def setUp(self):

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(
            chrome_options=chrome_options, executable_path='./chromedriver')

        user = User.objects.create_user('foo', password='boo')
        user.is_staff=True
        user.save()
        # self.driver = webdriver.Firefox()
    def tearDown(self):
        self.driver.quit()
        super().tearDown()
    def test_login(self):
        #Adil open his Firefox and the website
        self.driver.get(self.live_server_url)

         #Adil see the items in the page
        time.sleep(2)
        username = self.driver.find_element_by_name('username')
        password = self.driver.find_element_by_name('password1')
        submit_button = self.driver.find_element_by_name('submit_button')
        get_sign_in = self.driver.find_element_by_name('sign_in')

        username.send_keys('foo')
        password.send_keys('boo')
        submit_button.send_keys(Keys.ENTER)

        time.sleep(2)
        self.assertIn('Selamat datang, foo', self.driver.page_source)
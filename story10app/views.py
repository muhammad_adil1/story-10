from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib.auth import logout

def login(request):
    if request.POST:
        try:
            usrname = request.POST['username']
            pswrd = request.POST['password1']
            user = authenticate(username=usrname, password=pswrd)
            if user is not None:
                context = {'username': 'Selamat datang, ' + str(user)}
                return render(request, 'login.html', context)
        except:
            pass
        try:
            cek = request.POST['logout']
            logout(request) 
        except:
            pass            
    return render(request, 'login.html', {'username':''})

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return login(request)
    form = UserCreationForm()
    context = {'form':form}
    return render(request, 'signup.html', context)
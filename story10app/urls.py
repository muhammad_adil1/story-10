from django.urls import path
from story10app import views

urlpatterns = [
    path('', views.login),
    path('signup/', views.signup),
]